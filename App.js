//Importamos React
import React, { Component } from 'react';

//Importamos Iconos
import IconMaterial from 'react-native-vector-icons/MaterialCommunityIcons';

//Importamos Iconos
import Icon from 'react-native-vector-icons/MaterialIcons';

//Importamos ScrollableTabView
import ScrollableTabView from 'react-native-scrollable-tab-view';

//Importamos otros componentes de su carpeta
import Chats from './app/components/Chats';
import States from './app/components/States';
import Calls from './app/components/Calls';

//Importamos el FAB
import ActionButton from 'react-native-action-button';

//Importamos desde react las Views, Texts y los estilos
import {
	StyleSheet,
	View,
	Text
} from 'react-native';

//Codigo de aplicacion
export default class App extends Component {
	//Constructor
	constructor(props) {
		super(props)
		this.state = {
			pageNumber: 0
		} 
	}

	/*Funcion cambiar estado al cambiar de tab*/
	setStateForTabChange = (pageNumber) => {
		this.setState({
			pageNumber: pageNumber
		})
	 }
	 
	render() {
	return (
		<>
			{/*Contenedor principal*/}
			<View style={styles.mainContainer}>
				{/*Cabecera*/}
				<View style={styles.headerContainer}>
					{/*Texto Whatsapp*/}
					<View style={styles.leftHeaderContainer}>
						<Text style={styles.logo}>WhatsApp</Text>
					</View>
					{/*Search y mas opciones*/}
					<View style={styles.rightHeaderContainer}>
						<Icon name="search" color="#fff" size={27} style={{padding:10}} />
						<Icon name="more-vert" color="#fff" size={27} style={{padding:10}} />
					</View>
				</View>

				{/*Contenedor contenido*/}
				<View style={styles.contentContainer} >
					<ScrollableTabView
					initialPage={0}
					onChangeTab={(event)=>{this.setStateForTabChange(event.i)}}
					tabBarUnderlineIcon="#fff"
					tabBarUnderlineStyle={{backgroundColor: "#fff"}}
					tabBarActiveTextColor="#fff"
					tabBarInactiveTextColor="#ddd"
					tabBarBackgroundColor="#075e54">
						<Chats tabLabel="CHATS" />
						<States tabLabel="ESTADOS" />
						<Calls tabLabel="LLAMADAS" />
					</ScrollableTabView>
				</View>
		 	</View>

			{/*FAB*/}
			<ActionButton buttonColor="#25D366" offsetX={15} offsetY={15} renderIcon={active => active ? (<IconMaterial name="android-messages" size={24} style={{color:"#fff"}} /> ) : 
			(<IconMaterial name={(this.state.pageNumber == 0) ? "android-messages" : 
			(this.state.pageNumber == 1) ? "camera": 
			(this.state.pageNumber == 2) ? "phone-plus" : "minecraft"} 
			size={24} style={{color:"#fff"}} />)}> 
			</ActionButton>

			{/*Boton lapiz en States*/}
			<ActionButton buttonColor={(this.state.pageNumber == 1) ? "#f2f2f2":null} size={40} offsetX={23} offsetY={80}  renderIcon={active => active ? (<IconMaterial name="android-messages" size={24} style={{color:"#fff"}} /> ) : 
			(<IconMaterial name={(this.state.pageNumber == 1) ? "pencil":active=false} 
			size={20} style={{color:"#505050"}} />)}> 
			</ActionButton>
			
		</>
	);
};
}
const styles = StyleSheet.create({
	mainContainer: {
		flex: 1,
		backgroundColor: '#F5FCFF'
	},
	headerContainer: {
		flex: 1,
		flexDirection: "row",
		justifyContent: "space-between",
		backgroundColor: "#075e54",
		alignItems: "center"
	},
	contentContainer: {
		flex: 8
	},
	rightHeaderContainer: {
		flexDirection: "row",
		alignItems: "flex-end"
	},
	leftHeaderContainer: {
		flexDirection: "row",
		alignItems: "flex-start"
	},
	logo: {
		color: "#ffffff",
		fontSize: 20,
		marginLeft: 10,
		fontWeight: "bold"
	}, 
	actionButtonIcon: {
		fontSize: 20,
		height: 22,
		color: 'white',
	  }
});