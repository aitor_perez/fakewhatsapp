//Importamos React
import React, { Component } from 'react';

//Importamos Axios
import axios from 'axios';

//Importamos los datos falsos
import { FAKE_CALLS } from '../data/data';

//Importamos las calls
import ListCalls from '../components/ListCalls';

import {
 FlatList,
 ActivityIndicator
} from 'react-native';

export default class Calls extends Component {
  //Creamos constructor
	constructor(props) {
		super(props)
		this.state = {
		  callList : [],
      loaded: false,
      pageNumber: 2
		} 
	}
	
	//Peticiones http
	componentDidMount() {
    axios.get(FAKE_CALLS)
      .then((response) => {
        this.setState({
          callList: response.data,
          loaded: true
        })
      })
      .catch(function (error) {
        console.log(error);
      });
  } 
  
  render() {
    if(this.state.loaded) {
      return(
        <FlatList
         data={this.state.callList}
         renderItem={({item}) => (
           <ListCalls
             first_name={item.first_name}
             date={item.date}
             time={item.time}
             image={item.image}
             missed={item.missed}
             video_call={item.video_call}
           />
         )}
       />
      )
    } else {
      return(
        <ActivityIndicator size="large" />
      )
    }
  }
}
