//Importamos React
import React, { Component } from 'react';

//Importamos componente chat
import { GiftedChat } from 'react-native-gifted-chat';

//Empieza programa
export default class MyChat extends Component {
    state = {
        messages: []
      };

        onSend = (messages = []) => {
            const currentUser = {
                _id: 1,
                name: 'Developer',
            }
            messages.forEach(message => {
            currentUser
                .sendMessage({
                text: message.text,
                })
                .then(() => {})
                .catch(err => {
                console.log(err);
                });
            });
        };

        onReceive = data => {
            const { id, senderId, text, createdAt } = data;
            const incomingMessage = {
            _id: id,
            text: text,
            createdAt: new Date(createdAt),
            user: {
                _id: senderId,
                name: senderId,
                avatar:
                'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQmXGGuS_PrRhQt73sGzdZvnkQrPXvtA-9cjcPxJLhLo8rW-sVA',
            },
            };
        
            this.setState(previousState => ({
            messages: GiftedChat.append(previousState.messages, incomingMessage),
            }));
        };

        componentDidMount() {
            this.setState({
            messages: [
                {
                _id: 1,
                text: "I think we passed the first step of the tutorial. We will now need a Pusher account!",
                createdAt: new Date(),
                user: {
                    _id: 1,
                    name: "React Native",
                    avatar: "https://placeimg.com/140/140/any"
                }
                }
            ]
            });
        }

        render() {
            return (
                <GiftedChat
                messages={this.state.messages}
                onSend={messages => this.onSend(messages)}
                />
            );
          }
}
