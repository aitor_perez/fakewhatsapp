//Importamos React
import React, { Component } from 'react';

//Importamos Axios
import axios from 'axios';

//Importamos los datos falsos
import { FAKE_STATES } from '../data/data';

//Importamos las calls
import ListStates from '../components/ListStates';

import {
 FlatList,
 View,
 Image,
 Text,
 ActivityIndicator,
 StyleSheet
} from 'react-native';

export default class States extends Component {
  //Creamos constructor
	constructor(props) {
		super(props)
		this.state = {
		  statesList : [],
      loaded: false,
      pageNumber: 1
		} 
	}
	
	//Peticiones http
	componentDidMount() {
    axios.get(FAKE_STATES)
      .then((response) => {
        this.setState({
          statesList: response.data,
          loaded: true
        })
      })
      .catch(function (error) {
        console.log(error);
      });
  } 
  
  render() {
    if(this.state.loaded) {
      return(
        <View style={{flex: 0.82,flexDirection: 'column',justifyContent: 'space-between'}}>
          <View style = {styles.listItemContainer}>
              <View style= {styles.avatarContainer}>
                  <Image
                  style={styles.avatar}
                  source={{uri: "https://randomuser.me/api/portraits/lego/6.jpg"}} 
                  />
              </View>
              <View style = {styles.chatDetailsContainer}>
                  <View style={styles.chatDetailsContainerWrap}>
                      <View style={styles.nameContainer}>
                          <Text style={styles.nameText}>Mi estado</Text>
                          <Text style={styles.dateText}>añade una actualización</Text>
                      </View>
                  </View>
              </View>
          </View>
          
          <View style = {styles.separadorRecientes}>
              <Text style={styles.closeText}>Recientes</Text>
          </View>

          <FlatList
          data={this.state.statesList}
          renderItem={({item}) => (
            <ListStates
              first_name={item.first_name}
              date={item.date}
              time={item.time}
              image={item.image}
            />
          )}
        />
       </View>
      )
    } else {
      return(
        <ActivityIndicator size="large" />
      )
    }
  }
}

const styles = StyleSheet.create({
  listItemContainer: {
    flex: 1,
    flexDirection: "row",
    padding: 10,
    marginBottom:10
  },
  avatarContainer: {
    flex: 1,
    alignItems: "flex-start"
  },
  chatDetailsContainer: {
    flex: 4,
  },
  chatDetailsContainerWrap: {
    flex: 1,
    flexDirection: "row",
    padding: 5
  },
  nameContainer: {
    alignItems: "flex-start",
    flex: 1
  },
  separadorRecientes: {
    marginTop: 10,
    padding: 2,
    flexDirection: "row",
    backgroundColor: "#f5f4f4",
    borderTopColor: "rgba(92,94,94,0.5)",
    borderTopWidth: 0.25
  },
  nameText: {
    fontWeight: "bold",
    color: "#000",
    fontSize: 17
  },
  dateText: {
    fontSize: 15,
    color: "#787474"
  },
  avatar: {
    borderRadius: 30,
    width: 60,
    height: 60
  },
  closeText: {
    padding: 6,
    fontWeight: "bold",
    fontSize: 17,
    color: "#00827c"
  }
 });
