//Importamos React
import React, { Component } from 'react';

//Importamos iconos
import Icon from 'react-native-vector-icons/MaterialIcons';

//Importamos StyleSheets y Views
import {
    View,
    Image,
    Text,
    StyleSheet
} from 'react-native';

export default class ListChat extends Component {
    render(){
        return(
            <View style = {styles.listItemContainer}>
                <View style= {styles.avatarContainer}>
                    <Image
                    style={styles.avatar}
                    source={{uri: this.props.image}} 
                    />
                </View>
                <View style = {styles.chatDetailsContainer}>
                    <View style={styles.chatDetailsContainerWrap}>
                        <View style={styles.nameContainer}>
                            <Text style={styles.nameText}>{this.props.first_name}</Text>
                        </View>
                    </View>
                    <View style={styles.chatDetailsContainerWrap}>
                        <View style={styles.infoContainer}>
                            <Icon name={this.props.missed ? "call-missed" : "call-received"} color={this.props.missed ? "#ff3333" : "#075e54"} size={20} style={{marginRight:5,marginTop:10}} />
                        </View>
                        <View style={styles.dateContainer}>
                            <Text style={styles.dateText}>{this.props.date} {this.props.time}</Text>
                        </View>
                        <View style={styles.iconContainer}>
                            <Icon name={this.props.video_call ? "call" : "video-call"} color="#075e54" size={35} />
                        </View>
                    </View>
                </View>
            </View>
        )
    };
}

const styles = StyleSheet.create({
    listItemContainer: {
      flex: 1,
      flexDirection: "row",
      padding: 10
    },
    avatarContainer: {
      flex: 1,
      alignItems: "flex-start"
    },
    chatDetailsContainer: {
      flex: 5,
      borderBottomColor: "rgba(92,94,94,0.5)",
      borderBottomWidth: 0.25,
      marginTop: -10,
      marginBottom: 2
    },
    chatDetailsContainerWrap: {
      flex: 1,
      flexDirection: "row",
      padding: 5
    },
    iconContainer:{
      flex:1,
      flexDirection: "column",
      marginTop: -15,
      alignItems: "flex-end",
      marginBottom: 10
    },
    nameContainer: {
      flexDirection: "column",
      alignItems: "flex-start",
      flex: 1,
      marginBottom:-10
    },
    dateContainer: {
      flexDirection: "row",
      justifyContent : "space-between",
      alignItems: "flex-end"
    },
    nameText: {
      fontWeight: "bold",
      color: "#000",
      fontSize: 17
    },
    dateText: {
      fontSize: 15
    },
    avatar: {
      borderRadius: 30,
      width: 60,
      height: 60
    }
   });
