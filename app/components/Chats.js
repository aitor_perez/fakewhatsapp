//Importamos React
import React, { Component } from 'react';

//Importamos Axios
import axios from 'axios';

//Importamos los datos falsos
import { FAKE_CHATS } from '../data/data';

//Importamos los chats
import ListChat from '../components/ListChat';

//Importamos Iconos
import Icon from 'react-native-vector-icons/MaterialIcons';

//Importamos el chat
import MyChat from '../components/MyChat';

//Importamos textos y componentes desde React
import {
  FlatList,
  TouchableHighlight,
  View,
  Text,
  Modal,
  Image,
  StyleSheet,
  ActivityIndicator,
  ImageBackground
} from 'react-native';

export default class Chats extends Component {
  //Creamos constructor
	constructor(props) {
		super(props)
		this.state = {
      modalVisible: false,
		  chatList : [],
      loaded: false,
      pageNumber: 0,
		} 
  }
  
  //Cambio modal visible
  setModalVisible(visible) {
    this.setState({modalVisible: visible});
  }

  //Cambio modal invisible
  setModalInvisible(invisible) {
    this.setState({modalVisible: invisible});
  }
	
	//Peticiones http
	componentDidMount() {
    axios.get(FAKE_CHATS)
      .then((response) => {
        this.setState({
          chatList: response.data,
          loaded: true
        })
      })
      .catch(function (error) {
        console.log(error);
      });
  } 
  
  render() {
    if(this.state.loaded) {
      return(
        <View>
          <Modal
              animationType="fade"
              transparent={false}
              visible={this.state.modalVisible} 
          >
            
            {/*Contenedor principal*/}
            <View style={styles.mainContainer}>
            <ImageBackground 
              source={require('../../img/background.png')} 
              style={{width: '100%', height: '100%'}}>
              {/*Cabecera*/}
              <View style={styles.headerContainer}>
                {/*Arrow Back*/}
                <View style={styles.arrowBack} >
                  <TouchableHighlight
                    onPress={() => {
                      this.setModalInvisible(!this.state.modalVisible);
                    }}>      
                    <Icon name="arrow-back" color="#fff" size={27}  />  
                  </TouchableHighlight>
                </View>
                {/*Avatar y nombre*/}
                <View style={styles.leftHeaderContainer}>
                  <View style= {styles.avatarContainer}>
                    <Image
                    style={styles.avatar}
                    source={{uri: "https://randomuser.me/api/portraits/lego/6.jpg"}} 
                    />
                  </View>
                  <View style={styles.nameContainer}>
                    <Text style={styles.nameText}>nombre</Text>
                  </View>
                </View>
                {/*Search y mas opciones*/}
                <View style={styles.rightHeaderContainer}>
                  <Icon name="video-call" color="#fff" size={27} style={{padding:10}} />
                  <Icon name="phone" color="#fff" size={27} style={{padding:10}} />
                  <Icon name="more-vert" color="#fff" size={27} style={{padding:10}} />
                </View>
              </View>
              
              <View style={styles.contentContainer}>

                <MyChat />

              </View>
              
              </ImageBackground>
            </View>
          </Modal>
          <TouchableHighlight
            onPress={() => {
            this.setModalVisible(!this.state.modalVisible);
          }}>
                                                 
            <FlatList
              data={this.state.chatList}
              renderItem={({item}) => (
                <ListChat
                  first_name={item.first_name}
                  message={item.message}
                  image={item.image}
                  date={item.date}
                  time={item.time}
                />
              )}
            />

          </TouchableHighlight>
          
        </View>

                                                  /*<TouchableHighlight
                                                    onPress={() => {
                                                      this.setModalInvisible(!this.state.modalVisible);
                                                    }}>
                                                    <Text>Out Modal</Text>
                                                  </TouchableHighlight>*/
                                                  
                                                  /*<TouchableHighlight
                                                    onPress={() => {
                                                      this.setModalVisible(!this.state.modalVisible);
                                                    }}>
                                                    <Text>show Modal</Text>
                                                  </TouchableHighlight>*/


      )
    } else {
      return(
        <ActivityIndicator size="large" />
      )
    }
  }
}

const styles = StyleSheet.create({
  mainContainer: {
		flex: 1,
		backgroundColor: '#F5FCFF'
  },
  contentContainer: {
    flex: 8
  },
  chat:{
    flex: 1
  },  
	headerContainer: {
		flex: 1,
		flexDirection: "row",
		justifyContent: "space-between",
		backgroundColor: "#075e54",
		alignItems: "center"
	},
	contentContainer: {
		flex: 8
	},
	rightHeaderContainer: {
		flexDirection: "row",
		alignItems: "flex-end"
	},
	leftHeaderContainer: {
		flexDirection: "row",
		alignItems: "flex-start"
  },
  arrowBack: {
    marginRight: -80,
    marginLeft: 10
  },
  avatarContainer: {
    alignItems: "flex-start"
  },
  nameContainer:{
    alignItems: "flex-start",
    marginLeft: 10,
    marginTop: 10
  },
  avatar: {
    borderRadius: 30,
    width: 47,
    height: 47
  },
  nameText: {
    color: "#ffffff",
		fontSize: 18,
    fontWeight: "bold"
  }
});